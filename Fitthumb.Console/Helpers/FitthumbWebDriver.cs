﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Fitthumb.Console.Helpers
{
    public static class FitthumbWebDriver
    {
        private static IWebDriver _webDriver { get; set; }

        public static IWebDriver WebDriver
        {
            get
            {
                if (_webDriver != null) return _webDriver;

                var binPath = System.IO.Path.GetDirectoryName(
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                var chromeOptions = new ChromeOptions();
                _webDriver = new ChromeDriver(binPath.Substring(6), chromeOptions);

                return _webDriver;
            }
        }
    }
}