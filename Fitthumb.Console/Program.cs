﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Fitthumb.Console.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Fitthumb.Console
{
    class Program
    {
        private static IWebDriver _webDriver;
        private static Random _rnd;
        static void Main(string[] args)
        {
            var usernameText = ConfigurationManager.AppSettings["Fitthumb_username"];
            var passwordText = ConfigurationManager.AppSettings["Fitthumb_password"];

            if (usernameText == "ENTER_USERNAME_HERE" || passwordText == "ENTER_PASSWORD_HERE")
            {
                throw new ArgumentNullException("Please enter username and password in App.Config file");
            }

            _rnd = new Random();
            _webDriver = FitthumbWebDriver.WebDriver;

            _webDriver.Navigate().GoToUrl("https://login.fitthumb.com/user/home");
            _webDriver.Manage().Window.Maximize();

            var usernameField = _webDriver.FindElement(By.Id("plcMain_txtEmail"));
            var passwordField = _webDriver.FindElement(By.Id("plcMain_txtPassword"));
            var loginButton = _webDriver.FindElement(By.Id("plcMain_btnLogin"));

            usernameField.SendKeys(usernameText);
            passwordField.SendKeys(passwordText);
            loginButton.Click();


            const string dailyEntryUrl = "https://login.fitthumb.com/misc/homeEntryPanel?dateIn=";

            var beginningOfYear = new DateTime(DateTime.Now.Year, 1, 1);
            var currentDate = DateTime.UtcNow;

            DateTime activeDate = beginningOfYear;

            
            while (activeDate <= currentDate)
            {
                var dtString = activeDate.ToString("d");
                System.Console.WriteLine(dtString);
                _webDriver.Navigate().GoToUrl($"{dailyEntryUrl}{dtString}");

                SetField("water", 3, 7);
                SetField("sleep", 5, 9);
                SetField("fruit", 2, 4);
                SetField("veggie", 2, 4);

                try
                {
                    var emotionSlider = _webDriver.FindElement(By.CssSelector(".emotion-input > .ui-slider-handle"));
                    var actions = new Actions(_webDriver);
                    actions.DragAndDropToOffset(emotionSlider, 670, 0).Perform();
                }
                catch (Exception)
                {
                    // ignored
                }

                var waitSeconds = _rnd.Next(3, 7) * 1000;
                Thread.Sleep(waitSeconds);
                activeDate = activeDate.AddDays(1);
            }



            

            System.Console.WriteLine("Press any key to stop...");
            System.Console.ReadLine();
        }

        static void SetField(string fieldType, int minRange, int maxRange)
        {
            var waterField = _webDriver.FindElement(By.CssSelector(".diary-input.diary-text[data-type=\"" + fieldType + "\"]"));
            var waterValue = int.Parse(waterField.GetAttribute("aria-valuenow"));
            if (waterValue == 0)
            {
                waterField.Clear();
                waterField.SendKeys(_rnd.Next(minRange, maxRange).ToString());
            }
        }
    }
}
